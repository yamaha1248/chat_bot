import telebot
from telebot import types
from revChatGPT.V1 import Chatbot
from better_profanity import profanity

bot = telebot.TeleBot("6248358428:AAGOCbUOW6a07rVkY0ob_l5UihEmr3pjKOo")
s = "123456789("
s_hello = ["привет", "хай", "hello", "hi", "здорово", "здравствуй"]
timer = 10
def check_timer(timer):
    if timer == 10:
        return True
    else:
        return False

@bot.message_handler(commands=['start'])
def start(message):
    mess = f'Привет, {message.from_user.first_name} {message.from_user.last_name}'
    bot.send_message(message.chat.id, mess)

@bot.message_handler(commands=['stop'])
def stop(message):
    bot.send_message(message.chat.id, "бот закрыт")

@bot.message_handler(commands=['code'])
def code(message):
    bot.send_message(message.chat.id, "бот закрыт")




@bot.message_handler(commands=['website'])
def website(message):
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Яндекс", url="https://yandex.ru"))
    bot.send_message(message.chat.id, "Перейдите на сайт", reply_markup=markup)

@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(message.chat.id, "Меня зовут БИГМ БОТ, и я могу быть полезным\nKоманды:\n* /help (список запросов и команд)\n* /start (начало работы)\n* /website (открывает яндекс)\nКлючевые запросы:\n* photo (присылает фото)\n* hello\n* id (показыват id)\n* калькулятор (просто введите действие)\n* ? (если есть непаладки то сначала ? а потом неполадку)\n")

@bot.message_handler(content_types=['text'])

def get_user_text(message):
    censored = profanity.censor(message.text, '$')

    if '$' in censored:
        bot.send_message(message.chat.id, f"{message.from_user.first_name} {message.from_user.last_name}, ругаться плохо!")
        bot.delete_message(message.chat.id, message.id)
    if len(message.text) > 3:
        with open("logs/worlds.txt", "a", encoding="utf-8") as file:

            file_text = f"{message.from_user.username}:  {message.text}\n"
            file.write(file_text)
    message.text = message.text.lower()
    message.text = message.text.replace('!', '')

    if message.text in s_hello:
        mess = f'Привет, {message.from_user.first_name} {message.from_user.last_name}'
        bot.send_message(message.chat.id, mess)
    elif message.text == "id":
        bot.send_message(message.chat.id, f"твой ID: {message.from_user.id}")
    elif message.text == "photo":
        bot.ban_chat_sender_chat(message.chat.id, message.chat.id)
        # photo = open('images/img.png', 'rb')
        # bot.send_photo(message.chat.id, photo)
    elif message.text[0] == "?":
        bot.send_message(message.chat.id, "запрос отправлен")
    elif message.text[0] in s:
        try:
            calc = eval(message.text)
            bot.send_message(message.chat.id, calc)
        except SyntaxError:
            bot.send_message(message.chat.id, "неккоректный ввод!")

    else:
        bot.send_message(message.chat.id, "Я тебя не понимаю")


# @bot.message_handler(content_types=['photo'])
# def get_user_photo(message):
#     if message.text == ""
#     bot.send_message(message.chat.id, "Вау, крутое фото!")


bot.polling(none_stop=True)

